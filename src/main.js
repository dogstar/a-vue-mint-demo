import Vue from 'vue'
import App from './App.vue'
import router from './router'
import MintUI from 'mint-ui' // 加这一行
import 'mint-ui/lib/style.css' // 和加这一行

Vue.use(MintUI)   // 还有加这一行

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})