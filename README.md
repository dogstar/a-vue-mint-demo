# Vue + Vue-CLI + Mint-UI 移动端开发新手示例实战 - by dogstar

## 快速使用

``` bash
# 安装依赖
npm install

# 本地开发调试 localhost:8080
npm run dev

# 打包到生产环境
npm run build
```

## 重要参考资料

### Mint UI
饿了么出品的Mint UI - 基于 Vue.js 的移动端组件库（移动端专用）。  
官网文档：[http://mint-ui.github.io/docs/#/zh-cn2](http://mint-ui.github.io/docs/#/zh-cn2)  

### iView
最近很火的一套基于 Vue.js 的高质量UI 组件库（PC端专用）。  
官网文档：[https://www.iviewui.com/](https://www.iviewui.com/)  

## 新手开始讲故事

这里主要使用了：  

 + npm：主流的包依赖管理工具  
 + vue-cli：开发辅助工具，集成webpack等打包和本地开发工具  
 + mint-ui：专门用来做移动端开发的UI

### 1、安装npm
打开nodejs的官网：https://nodejs.org/en/，然后，选择合适的版本下载。  
例如这里下载的是windows 64位版：https://nodejs.org/dist/latest/node-v11.2.0-x64.msi   

然后，一路点击，即可安装完毕。  

成功安装后，就有node和npm这两个东西可以使用了，查看下版本号：  
```
$ node -v
v11.2.0

$ npm -v
6.4.1
```

### 2、安装vue-cli
Vue CLI 3的官网是：https://cli.vuejs.org/ ，其slogan为：Standard Tooling for Vue.js Development 。  

安装vue-cli很简单，参考：[Installation](https://cli.vuejs.org/guide/installation.html)。  
```
$ npm install -g vue-cli
```

安装后，查看版本号：  
```
$ vue -V
3.1.3
```

### 3、创建一个项目
在你想创建项目的目录位置，运行：  
```
$ vue init webpack demo

? Project name (demo)
```
就会捍到一连串的提示输入，主要是项目名称、作者名字、项目描述等。  

填写完毕后，就可以尝试运行一下demo了：  
```
$ npm run dev
```

接着可以看到熟悉的页面。  
// todo


### 4、使用Mint-UI
现在，开始引入Mint-UI，Mint-UI官网为：https://mint-ui.github.io/#!/zh-cn 。它的安装也很简单，在刚才的新项目根目录下，运行：  
```
$ npm install --save mint-ui
```

然后，把./src/main.js里面的内容，追加Mint-UI的相关代码，即改为：  
```
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import MintUI from 'mint-ui' // 加这一行
import 'mint-ui/lib/style.css' // 和加这一行

Vue.use(MintUI)   // 还有加这一行

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
```

若在保持本地运行的话，追加上面代码后会自动更新构建，一切正常！  
```
 WAIT  Compiling...17:30:50

 95% emitting DONE  Compiled successfully in 153ms17:30:50

 I  Your application is running here: http://localhost:8080
```

### 5、添加一个自己的新页面

那怎么添加一个自己的新页面呢？  

先在./src/router/index.js文件中，把相关的配置改：  
```
export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/hi',
      name: 'Hi',
      component: () => import('@/view/hi/hi.vue')
    }
  ]
})
```
这里追加了```/hi```的页面路由配置，关于router，官方文档是：https://router.vuejs.org/ 。  

别忘了，与此同时，或者在这之前，创建对应的Vue页面，例如这里新建文件 ```./src/view/hi/hi.vue```，并在里面按照模板、到JS、到样式的顺序，放置你自己的代码，例如：  
```
<template>
  <div id="hi">
    <mt-header title="标题过长会隐藏后面的内容啊哈哈哈哈">
      <router-link to="/" slot="left">
        <mt-button icon="back">返回</mt-button>
      </router-link>
      <mt-button icon="more" slot="right"></mt-button>
    </mt-header>
  </div>
</template>

<script>
/* eslint-disable */

import { Header } from 'mint-ui'

export default {
  name: 'Hi',
  components: {
    'mt-header': Header
  },
  data () {
    return {
    }
  }
}
</script>

<style scoped>
</style>
```

完成后，就可以通过以下链接访问到页面了。  

http://localhost:8080/#/hi

效果如下：  
//todo

### 6、如何修改默认布局？  

可以看到，上面默认的首页，以及新页面，都会有一个很大的Vue的Logo图片，以及会有顶部的距离。怎么去掉呢？  

很简单，修改文件 ```./src/App.vue```，内容改为：  
```
<template>
  <div id="app">
    <router-view/>
  </div>
</template>

<script>
export default {
  name: 'App'
}
</script>

<style>
#app {
  font-family: 'Avenir', Helvetica, Arial, sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  text-align: center;
  color: #2c3e50;
  margin-top: 0px;
}
</style>
```

### 7、取消严格的语法检测

在默认情况下，会进行严格的语法检测，经常会出现类似以下这样的信息（例如缩进不好）：

```
  http://eslint.org/docs/rules/no-mixed-spaces-and-tabs  Mixed spaces and tabs
  src\view\hi\hi.vue:6:2
          </router-link>
         ^
```

这时，如果想去掉，可以修改 ```./build/webpack.base.conf.js```文件，去掉里面的useEslint规则，如下面注释掉的这一行。  

```
  module: {
    rules: [
      //...(config.dev.useEslint ? [createLintingRule()] : []),
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: vueLoaderConfig
      },
```

然后重新本地运行即可。  

### 8、修改打包后的路径

在开发完成后，就可以打包，准备发布到生产环境进行正式访问了。但会有一个小问题，如果生产环境的位置不是在网站根目录的话，就会导致资源加载不到。这时，可以修改 ```./config/index.js```文件中的assetsPublicPath配置，改为相应的相对路径。例如是dist，就改为：```assetsPublicPath: '/dist/'```，如下：  

```
  build: {
    // Template for index.html
    index: path.resolve(__dirname, '../dist/index.html'),

    // Paths
    assetsRoot: path.resolve(__dirname, '../dist'),
    assetsSubDirectory: 'static',
    assetsPublicPath: '/dist/',
```

再次打包发布后，就能找到相关的静态文件了。  

### 9、TODO：Network访问？  

